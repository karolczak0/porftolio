var fs = require('fs');
var path = require('path');
var express = require('express');
var router = express.Router();

if (!String.prototype.endsWith) {
    Object.defineProperty(String.prototype, 'endsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || this.length;
            position = position - searchString.length;
            var lastIndex = this.lastIndexOf(searchString);
            return lastIndex !== -1 && lastIndex === position;
        }
    });
}

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
}

function getImages(srcpath,projectpath) {
  return fs.readdirSync(srcpath + projectpath).filter(function(file) {
    return file.endsWith('.jpg') ||  file.endsWith('.png');
  }).map(function(file) {
	  return path.join(srcpath, projectpath, file).replace(/\\/g, '/').replace("public/","");
  });
}

function getProjects(req, res, projectpath) {
	var dirs = getDirectories(projectpath);
	var projects = dirs.map(function (dir){
		project = JSON.parse(fs.readFileSync(projectpath + dir + '/project_info.json', 'utf8')).project;
		project.images = getImages(projectpath, dir);
		return project;
	});
	res.send(projects);
}

router.get('/gamedev', function(req, res) {getProjects(req,res,'public/projects-gamedev/')})
router.get('/science', function(req, res) {getProjects(req,res,'public/projects-science/')})


module.exports = router
