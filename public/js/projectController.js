var portfolio = angular.module('portfolio', []);

portfolio.controller("projectsGamedevController", 

	function ($scope, $http) {

		$scope.formData = {};
		
		 $http.get('/api/projects/gamedev')
			.success(function(data) {
				$scope.projects = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
			
	}
);
portfolio.controller("projectsScienceController", 

	function ($scope, $http) {

		$scope.formData = {};
		
		 $http.get('/api/projects/science')
			.success(function(data) {
				$scope.projects = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
			
	}
);